/**
  ******************************************************************************
  * @file    usbh_usr.c
  * @author  MCD Application Team
  * @version V1.2.0
  * @date    09-November-2015
  * @brief   This file includes the usb host library user callbacks
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "usbh_usr.h"
#include "ff.h"       /* FATFS */
#include "usbh_msc_core.h"
#include "usbh_msc_scsi.h"
#include "usbh_msc_bot.h"
#include "hw.h"

extern USB_OTG_CORE_HANDLE          USB_OTG_Core;

uint8_t USBH_USR_ApplicationState = USH_USR_FS_INIT;
uint8_t filenameString[15]  = {0};

#define FILE_BUFFER_SIZE 64
FATFS fatfs;
FIL file;
char file_buf[FILE_BUFFER_SIZE+1];


/*  Points to the DEVICE_PROP structure of current device */
/*  The purpose of this register is to speed up the execution */

USBH_Usr_cb_TypeDef USR_cb =
{
	USBH_USR_Init,
	USBH_USR_DeInit,
	USBH_USR_DeviceAttached,
	USBH_USR_ResetDevice,
	USBH_USR_DeviceDisconnected,
	USBH_USR_OverCurrentDetected,
	USBH_USR_DeviceSpeedDetected,
	USBH_USR_Device_DescAvailable,
	USBH_USR_DeviceAddressAssigned,
	USBH_USR_Configuration_DescAvailable,
	USBH_USR_Manufacturer_String,
	USBH_USR_Product_String,
	USBH_USR_SerialNum_String,
	USBH_USR_EnumerationDone,
	USBH_USR_UserInput,
	USBH_USR_MSC_Application,
	USBH_USR_DeviceNotSupported,
	USBH_USR_UnrecoveredError
};

/**
* @}
*/

/** @defgroup USBH_USR_Private_Constants
* @{
*/ 
/*--------------- LCD Messages ---------------*/
const char MSG_HOST_INIT[]        = "Host Library Initialized\r\n";
const char MSG_DEV_ATTACHED[]     = "Device Attached \r\n";
const char MSG_DEV_DISCONNECTED[] = "Device Disconnected\r\n";
const char MSG_DEV_ENUMERATED[]   = "Enumeration completed \r\n";
const char MSG_DEV_HIGHSPEED[]    = "High speed device detected\r\n";
const char MSG_DEV_FULLSPEED[]    = "Full speed device detected\r\n";
const char MSG_DEV_LOWSPEED[]     = "Low speed device detected\r\n";
const char MSG_DEV_ERROR[]        = "Device fault \r\n";

const char MSG_MSC_CLASS[]        = "Mass storage device connected\r\n";
const char MSG_HID_CLASS[]        = "HID device connected\r\n";
const char MSG_DISK_SIZE[]        = "Size of the disk in MBytes: \r\n";
const char MSG_LUN[]              = "LUN Available in the device:\r\n";
const char MSG_ROOT_CONT[]        = "Exploring disk flash ...\r\n";
const char MSG_WR_PROTECT[]       = "The disk is write protected\r\n";
const char MSG_UNREC_ERROR[]      = "UNRECOVERED ERROR STATE\r\n";

/**
* @}
*/


/** @defgroup USBH_USR_Private_FunctionPrototypes
* @{
*/
static uint8_t Explore_Disk (char* path , uint8_t recu_level);
static uint8_t FileBrowser (char* path);
static void    ShowFile(void);
void toggle_leds(void);
/**
* @}
*/ 


/** @defgroup USBH_USR_Private_Functions
* @{
*/ 

#ifdef STM32F4_DISCOVERY
// Discovery board has a pull-down with the switch to 3.3V
#define PRESSED (SET)
#define NOT_PRESSED (RESET)
#else
// Waveshare board has switch to GND and we enable a pull-up on the pin
#define PRESSED (RESET)
#define NOT_PRESSED (SET)
#endif

void wait_for_key(void)
{
	// Wait for press
	while ((HCD_IsDeviceConnected(&USB_OTG_Core)) && (gpio_get_pin(&pb_pa0) == NOT_PRESSED))
	{
		toggle_leds();
		USB_OTG_BSP_mDelay(100);
	}
	// Wait for release
	while ((HCD_IsDeviceConnected(&USB_OTG_Core)) && (gpio_get_pin(&pb_pa0) == PRESSED))
	{
		toggle_leds();
		USB_OTG_BSP_mDelay(100);
	}
}

/**
* @brief  USBH_USR_Init 
*         Displays the message on LCD for host lib initialization
* @param  None
* @retval None
*/
void USBH_USR_Init(void)
{
	static uint8_t startup = 0;

	if (startup == 0 )
	{
		startup = 1;

#ifdef USE_USB_OTG_HS 
		printf(" USB OTG HS MSC Host\r\n");
#else
		printf(" USB OTG FS MSC Host\r\n");
#endif
		printf("USB Host library started.\r\n");
		printf("     USB Host Library v2.2.0\r\n" );
	}
}

/**
* @brief  USBH_USR_DeviceAttached 
*         Displays the message on LCD on device attached
* @param  None
* @retval None
*/
void USBH_USR_DeviceAttached(void)
{
	printf(MSG_DEV_ATTACHED);
}


/**
* @brief  USBH_USR_UnrecoveredError
* @param  None
* @retval None
*/
void USBH_USR_UnrecoveredError (void)
{
	/* Set default screen color*/
	printf(MSG_UNREC_ERROR);
}


/**
* @brief  USBH_DisconnectEvent
*         Device disconnect event
* @param  None
* @retval Status
*/
void USBH_USR_DeviceDisconnected (void)
{
	printf(MSG_DEV_DISCONNECTED);
}
/**
* @brief  USBH_USR_ResetUSBDevice 
* @param  None
* @retval None
*/
void USBH_USR_ResetDevice(void)
{
	/* callback for USB-Reset */
}


/**
* @brief  USBH_USR_DeviceSpeedDetected 
*         Displays the message on LCD for device speed
* @param  Device speed
* @retval None
*/
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed)
{
	if (DeviceSpeed == HPRT0_PRTSPD_HIGH_SPEED)
	{
		printf(MSG_DEV_HIGHSPEED);
	}
	else if (DeviceSpeed == HPRT0_PRTSPD_FULL_SPEED)
	{
		printf(MSG_DEV_FULLSPEED);
	}
	else if (DeviceSpeed == HPRT0_PRTSPD_LOW_SPEED)
	{
		printf(MSG_DEV_LOWSPEED);
	}
	else
	{
		printf(MSG_DEV_ERROR);
	}
}

/**
* @brief  USBH_USR_Device_DescAvailable 
*         Displays the message on LCD for device descriptor
* @param  device descriptor
* @retval None
*/
void USBH_USR_Device_DescAvailable(void *DeviceDesc)
{ 
	USBH_DevDesc_TypeDef *hs;
	hs = DeviceDesc;

	printf("VID : %04lX\r\n" , (uint32_t)(*hs).idVendor);
	printf("PID : %04lX\r\n" , (uint32_t)(*hs).idProduct);
}

/**
* @brief  USBH_USR_DeviceAddressAssigned 
*         USB device is successfully assigned the Address 
* @param  None
* @retval None
*/
void USBH_USR_DeviceAddressAssigned(void)
{
}


/**
* @brief  USBH_USR_Conf_Desc 
*         Displays the message on LCD for configuration descriptor
* @param  Configuration descriptor
* @retval None
*/
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc,
                                          USBH_InterfaceDesc_TypeDef *itfDesc,
                                          USBH_EpDesc_TypeDef *epDesc)
{
  USBH_InterfaceDesc_TypeDef *id;
  
  id = itfDesc;  
  
  if ((*id).bInterfaceClass  == 0x08)
  {
	  printf(MSG_MSC_CLASS);
  }
  else if ((*id).bInterfaceClass  == 0x03)
  {
	  printf(MSG_HID_CLASS);
  }    
}

/**
* @brief  USBH_USR_Manufacturer_String 
*         Displays the message on LCD for Manufacturer String 
* @param  Manufacturer String 
* @retval None
*/
void USBH_USR_Manufacturer_String(void *ManufacturerString)
{
	printf("Manufacturer : %s\r\n", (char *)ManufacturerString);
}

/**
* @brief  USBH_USR_Product_String 
*         Displays the message on LCD for Product String
* @param  Product String
* @retval None
*/
void USBH_USR_Product_String(void *ProductString)
{
	printf("Product : %s\r\n", (char *)ProductString);
}

/**
* @brief  USBH_USR_SerialNum_String 
*         Displays the message on LCD for SerialNum_String 
* @param  SerialNum_String 
* @retval None
*/
void USBH_USR_SerialNum_String(void *SerialNumString)
{
	printf( "Serial Number : %s\r\n", (char *)SerialNumString);
} 



/**
* @brief  EnumerationDone 
*         User response request is displayed to ask application jump to class
* @param  None
* @retval None
*/
void USBH_USR_EnumerationDone(void)
{
	/* Enumeration complete */
	printf(MSG_DEV_ENUMERATED);
} 


/**
* @brief  USBH_USR_DeviceNotSupported
*         Device is not supported
* @param  None
* @retval None
*/
void USBH_USR_DeviceNotSupported(void)
{
	printf ("No registered class for this device. \n\r");
}  


/**
* @brief  USBH_USR_UserInput
*         User Action for application state entry
* @param  None
* @retval USBH_USR_Status : User response for key button
*/
USBH_USR_Status USBH_USR_UserInput(void)
{
	printf("Press key to continue...\r\n");
	wait_for_key();
	return USBH_USR_RESP_OK;
}  

/**
* @brief  USBH_USR_OverCurrentDetected
*         Over Current Detected on VBUS
* @param  None
* @retval Status
*/
void USBH_USR_OverCurrentDetected (void)
{
	printf("Overcurrent detected.\r\n");
}


/**
* @brief  USBH_USR_MSC_Application 
*         Demo application for mass storage
* @param  None
* @retval Status
*/
int USBH_USR_MSC_Application(void)
{
	FRESULT res;
	uint8_t writeTextBuff[] = "STM32 Connectivity line Host Demo application using FAT_FS   ";
	UINT bytesWritten, bytesToWrite;

	switch (USBH_USR_ApplicationState)
	{
	case USH_USR_FS_INIT:
		/* Initialises the File System*/
		if ( f_mount(&fatfs, "", 0) != FR_OK )
		{
			/* efs initialisation fails*/
			printf("Cannot initialize File System.\r\n");
			return(-1);
		}
		printf("File System initialized.\r\n");
		printf("Disk capacity : %d Bytes\r\n", (int)(USBH_MSC_Param.MSCapacity * USBH_MSC_Param.MSPageLength));

		if(USBH_MSC_Param.MSWriteProtect == DISK_WRITE_PROTECTED)
		{
			printf(MSG_WR_PROTECT);
		}

		USBH_USR_ApplicationState = USH_USR_FS_READLIST;
		break;

	case USH_USR_FS_READLIST:
		printf(MSG_ROOT_CONT);
		Explore_Disk("0:/", 1);
		USBH_USR_ApplicationState = USH_USR_FS_WRITEFILE;
		break;

	case USH_USR_FS_WRITEFILE:
		printf("Press Key to write file\r\n");
		wait_for_key();
		/* Writes a text file, STM32.TXT in the disk*/
		printf("Writing File to disk flash ...\r\n");
		if(USBH_MSC_Param.MSWriteProtect == DISK_WRITE_PROTECTED)
		{
			printf( "Disk flash is write protected \r\n");
			USBH_USR_ApplicationState = USH_USR_FS_DRAW;
			break;
		}
		/* Register work area for logical drives */
		f_mount(&fatfs, "", 0);
		if(f_open(&file, "0:STM32.TXT",FA_CREATE_ALWAYS | FA_WRITE) == FR_OK)
		{
			/* Write buffer to file */
			bytesToWrite = sizeof(writeTextBuff);
			res= f_write (&file, writeTextBuff, bytesToWrite, (void *)&bytesWritten);

			if((bytesWritten == 0) || (res != FR_OK)) /*EOF or Error*/
			{
				printf("STM32.TXT CANNOT be writen.\r\n");
			}
			else
			{
				printf("'STM32.TXT' file created\r\n");
			}

			/*close file and filesystem*/
			f_close(&file);
			f_mount(NULL, "", 0);
		}
		else
		{
			printf ("STM32.TXT created in the disk\r\n");
		}
		USBH_USR_ApplicationState = USH_USR_FS_DRAW;
		printf("To start file listing Press Key.\r\n");
		break;

	case USH_USR_FS_DRAW:

		printf("Waiting for key press\r\n");
		wait_for_key();

		while (HCD_IsDeviceConnected(&USB_OTG_Core))
		{
			if ( f_mount( &fatfs, "", 0) != FR_OK )
			{
				/* fat_fs initialisation fails*/
				return(-1);
			}
			return FileBrowser("0:/");
		}
		break;
	default:
		break;
	}
	return 0;
}

/**
* @brief  Explore_Disk 
*         Displays disk content
* @param  path: pointer to root path
* @retval None
*/
static uint8_t Explore_Disk (char* path , uint8_t recu_level)
{
	FRESULT res;
	FILINFO fno;
	DIR dir;
	char *fn;
	char tmp[14];

	res = f_opendir(&dir, path);
	if (res == FR_OK)
	{
		while (HCD_IsDeviceConnected(&USB_OTG_Core))
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0)
			{
				break;
			}
			if (fno.fname[0] == '.')
			{
				continue;
			}
			fn = fno.fname;
			strcpy(tmp, fn);
			if (recu_level == 1)
			{
				printf("   |__");
			}
			else if (recu_level == 2)
			{
				printf("   |   |__");
			}
			if ((fno.fattrib & AM_MASK) == AM_DIR)
			{
				strcat(tmp, "\n");
				printf(tmp);
			}
			else
			{
				strcat(tmp, "\n");
				printf(tmp);
			}
			if (((fno.fattrib & AM_MASK) == AM_DIR)&&(recu_level == 1))
			{
				Explore_Disk(fn, 2);
			}
		}
	}
	return res;
}

// Find and dump all the text files on the disk
static uint8_t FileBrowser (char* path)
{
	FRESULT res;
	uint8_t ret = 1;
	FILINFO fno;
	DIR dir;
	char *fn;

	res = f_opendir(&dir, path);
	if (res == FR_OK)
	{
		for (;;)
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0)
				break;
			if (fno.fname[0] == '.')
				continue;
			fn = fno.fname;
			if (fno.fattrib & AM_DIR)
			{
				continue;
			}
			else
			{
				if ((strstr(fn, "txt")) || (strstr(fn, "TXT")))
				{
					printf("Show file: %s\r\n", fno.fname);
					res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
					ShowFile();
					USB_OTG_BSP_mDelay(100);
					ret = 0;
					printf("\r\nPress key to continue...\r\n");
					wait_for_key();
					f_close(&file);
					printf("\r\n");
				}
			}
		}
	}

	printf("Disk capacity : %d Bytes\r\n", (int)(USBH_MSC_Param.MSCapacity * USBH_MSC_Param.MSPageLength));
	USBH_USR_ApplicationState = USH_USR_FS_READLIST;
	return ret;
}

/**
* @brief  ShowFile
*         Print the open text file to the serial port
* @param  None
* @retval None
*/
static void ShowFile(void)
{
	uint16_t numOfReadBytes = 0;
	FRESULT res;

	while (HCD_IsDeviceConnected(&USB_OTG_Core))
	{
		res = f_read(&file, file_buf, FILE_BUFFER_SIZE, (void *)&numOfReadBytes);
		if((numOfReadBytes == 0) || (res != FR_OK)) /*EOF or Error*/
		{
			break;
		}
		file_buf[numOfReadBytes] = 0;
		printf(file_buf);
	}
}


/**
* @brief  USBH_USR_DeInit
*         Deinit User state and associated variables
* @param  None
* @retval None
*/
void USBH_USR_DeInit(void)
{
	USBH_USR_ApplicationState = USH_USR_FS_INIT;
}


/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

