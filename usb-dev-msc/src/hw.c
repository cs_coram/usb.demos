/**
 * @file hw.c
 *
 * @brief hw setup for the uitla blade project
 *
 * @author OT
 *
 * @date Oct 2017
 *
 */

#include <hal.h>

	#include <stm32f4xx_conf.h>
	#include <gpio_hw.h>
	#include <dma_hw.h>
	#include <uart_hw.h>

    // Debug UART
    static gpio_pin_t uart2_rx_pa3 = {GPIOA, {GPIO_Pin_3, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL}, 7};
    static gpio_pin_t uart2_tx_pa2 = {GPIOA, {GPIO_Pin_2, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL}, 7};

    static dma_t uart2_rx_dma =
    {
        .stream = DMA1_Stream5,
        .channel = DMA_Channel_4,
    };

    static dma_t uart2_tx_dma =
    {
        .stream = DMA1_Stream6,
        .channel = DMA_Channel_4,
    };

    uart_t debug_uart =
    {
        .channel = USART2,

        .rx = &uart2_rx_pa3,
        .tx = &uart2_tx_pa2,

        .cfg = {
            .USART_BaudRate = 115200,
            .USART_WordLength = USART_WordLength_8b,
            .USART_StopBits = USART_StopBits_1,
            .USART_Parity = USART_Parity_No,
            .USART_Mode = USART_Mode_Rx | USART_Mode_Tx,
            .USART_HardwareFlowControl = USART_HardwareFlowControl_None,
        },

        .rx_dma = &uart2_rx_dma,
        .tx_dma = &uart2_tx_dma,
    };

	/**
	 * LEDs
	 */

	// Green LED - D12
	gpio_pin_t green_led_pd12 = {
		.port = GPIOD,
		.cfg = {.GPIO_Pin = GPIO_Pin_12, .GPIO_Mode = GPIO_Mode_OUT,
				.GPIO_Speed = GPIO_Speed_50MHz, .GPIO_OType = GPIO_OType_PP},
	};

	// Red LED - D14
	gpio_pin_t red_led_pd14 = {
		.port = GPIOD,
		.cfg = {.GPIO_Pin = GPIO_Pin_14, .GPIO_Mode = GPIO_Mode_OUT,
				.GPIO_Speed = GPIO_Speed_50MHz, .GPIO_OType = GPIO_OType_PP},
	};

	// Blue LED - D15
	gpio_pin_t blue_led_pd15 = {
		.port = GPIOD,
		.cfg = {.GPIO_Pin = GPIO_Pin_15, .GPIO_Mode = GPIO_Mode_OUT,
				.GPIO_Speed = GPIO_Speed_50MHz, .GPIO_OType = GPIO_OType_PP},
	};

	// Orange LED D13
	gpio_pin_t orange_led_pd13 = {
		.port = GPIOD,
		.cfg = {.GPIO_Pin = GPIO_Pin_13, .GPIO_Mode = GPIO_Mode_OUT,
				.GPIO_Speed = GPIO_Speed_50MHz, .GPIO_OType = GPIO_OType_PP},
	};

	// Push button
	gpio_pin_t pb_pa0 = {
		.port = GPIOA,
		.cfg = {.GPIO_Pin = GPIO_Pin_0, .GPIO_Mode = GPIO_Mode_IN,
				.GPIO_Speed = GPIO_Speed_50MHz, .GPIO_PuPd = GPIO_PuPd_NOPULL},
	};
