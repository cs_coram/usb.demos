#include "stm32f4xx.h"
#include "hal.h"
#include "hw.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "debug.h"

// ST USB driver
#include "usbd_msc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usb_dcd_int.h"


#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */

__ALIGN_BEGIN USB_OTG_CORE_HANDLE     USB_OTG_dev __ALIGN_END ;


void toggle_pin(gpio_pin_t *pin)
{
	gpio_set_pin(pin, !gpio_get_pin(pin));
}

void orange_on(void)
{
	gpio_set_pin(&orange_led_pd13, 1);
}

void orange_off(void)
{
	gpio_set_pin(&orange_led_pd13, 0);
}

int main(void)
{
	uint32_t timer;
	uint32_t now;
	int rx_count = 0;
	int tx_count = 0;
	uint32_t display_timer;

	/*!< At this stage the microcontroller clock setting is already configured,
	   this is done through SystemInit() function which is called from startup
	   files (startup_stm32f40_41xxx.s/startup_stm32f427_437xx.s/startup_stm32f429_439xx.s)
	   before to branch to application main.
	   To reconfigure the default setting of SystemInit() function, refer to
	   system_stm32f4xx.c file.
	 */
	sys_init();
	debug_out_init();

	// Heartbeat
	gpio_init_pin(&green_led_pd12);
	gpio_set_pin(&green_led_pd12, 0);
	// USB Tx busy
	gpio_init_pin(&orange_led_pd13);
	gpio_set_pin(&orange_led_pd13, 0);
	// RX data indicator
	gpio_init_pin(&red_led_pd14);
	gpio_set_pin(&red_led_pd14, 0);
	// USB Tx fail
	gpio_init_pin(&blue_led_pd15);
	gpio_set_pin(&blue_led_pd15, 0);


	// User push button PA0
	gpio_init_pin(&pb_pa0);


	  USBD_Init(&USB_OTG_dev,
	#ifdef USE_USB_OTG_HS
	            USB_OTG_HS_CORE_ID,
	#else
	            USB_OTG_FS_CORE_ID,
	#endif
	            &USR_desc,
	            &USBD_MSC_cb,
	            &USR_cb);

	/* Output a message on Hyperterminal using printf function */
	debug_printf("\n\rStart up USB Device MSC ram disk demo\n\r");

	// Wait for the message to be sent to the UART
	sys_spin(1000);


	timer = sys_get_tick();
	display_timer = timer;

	while (1)
	{
		now = sys_get_tick();
		if ((uint32_t)(now - timer) > 50)
		{
			toggle_pin(&green_led_pd12);
			timer = now;
		}
		// Display the receive and transmit counts every2 s
		if ((uint32_t)(now - display_timer) > 2000)
		{
			debug_printf("Rx %d Tx %d\r\n", rx_count, tx_count);
			display_timer = now;
			rx_count = 0;
			tx_count = 0;
		}
	}
}

void OTG_FS_IRQHandler(void)
{
    USBD_OTG_ISR_Handler(&USB_OTG_dev);
}
