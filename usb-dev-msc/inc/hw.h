/**
 * @file hw.h
 *
 * @brief hardware available on the uitla blade board
 *
 * @author OT
 *
 * @date Oct 2017
 *
 */

#ifndef __HW__
#define __HW__

#include <stdbool.h>
#include <gpio.h>
#include <uart.h>

// debug
extern gpio_pin_t heartbeat;
extern uart_t debug_uart;
extern gpio_pin_t green_led_pd12;
extern gpio_pin_t red_led_pd14;
extern gpio_pin_t blue_led_pd15;
extern gpio_pin_t orange_led_pd13;
extern gpio_pin_t pb_pa0;

#endif
