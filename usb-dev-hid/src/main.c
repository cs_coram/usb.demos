#include "stm32f4xx.h"
#include "hal.h"
#include "hw.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "debug.h"
#include "usb_comms.h"

// Unique ID register
#define         DEVICE_ID1          (0x1FFF7A14)

extern usb_dev_handle_t usb_hid_device;

void toggle_pin(gpio_pin_t *pin)
{
	gpio_set_pin(pin, !gpio_get_pin(pin));
}

void orange_on(void)
{
	gpio_set_pin(&orange_led_pd13, 1);
}

void orange_off(void)
{
	gpio_set_pin(&orange_led_pd13, 0);
}

int main(void)
{
	uint32_t timer;
	uint32_t now;
	uint8_t buf[66];
	int len;
	int rx_count = 0;
	int tx_count = 0;
	uint32_t display_timer;

	/*!< At this stage the microcontroller clock setting is already configured,
	   this is done through SystemInit() function which is called from startup
	   files (startup_stm32f40_41xxx.s/startup_stm32f427_437xx.s/startup_stm32f429_439xx.s)
	   before to branch to application main.
	   To reconfigure the default setting of SystemInit() function, refer to
	   system_stm32f4xx.c file.
	 */
	sys_init();
	debug_out_init();

	// Heartbeat
	gpio_init_pin(&green_led_pd12);
	gpio_set_pin(&green_led_pd12, 0);
	// USB Tx busy
	gpio_init_pin(&orange_led_pd13);
	gpio_set_pin(&orange_led_pd13, 0);
	// RX data indicator
	gpio_init_pin(&red_led_pd14);
	gpio_set_pin(&red_led_pd14, 0);
	// USB Tx fail
	gpio_init_pin(&blue_led_pd15);
	gpio_set_pin(&blue_led_pd15, 0);


	// User push button PA0
	gpio_init_pin(&pb_pa0);

	/* Output a message on Hyperterminal using printf function */
	debug_printf("\n\rStart up USB Device HID demo\n\r");

	// Wait for the message to be sent to the UART
	sys_spin(1000);

	usbcomms_init("01234");

	timer = sys_get_tick();
	display_timer = timer;

	while (1)
	{
		len = usb_hid_read(usb_hid_device, HID_OUT_REPORT_ID, buf, sizeof(buf));
		if (len)
		{
			toggle_pin(&red_led_pd14);
			rx_count++;
			// Terminate the string
			buf[len] = 0;
			// Display the received string
			//debug_printf("Rx: %s\r\n", buf);
		}
		now = sys_get_tick();
		if ((uint32_t)(now - timer) > 50)
		{
			toggle_pin(&green_led_pd12);
			timer = now;
			sprintf((char *)buf, "%d", tx_count);
			len = strlen((char *)buf);
			int ret = usb_hid_send_report(usb_hid_device, HID_IN_REPORT_ID, buf, len);
			if (ret == USB_RC_OK)
			{
				tx_count++;
				gpio_set_pin(&orange_led_pd13, 0);
				toggle_pin(&blue_led_pd15);
			}
			else
			{
				// Cannot send a new packet yet.
				gpio_set_pin(&orange_led_pd13, 1);
			}
		}
		// Display the receive and transmit counts every2 s
		if ((uint32_t)(now - display_timer) > 2000)
		{
			debug_printf("Rx %d Tx %d\r\n", rx_count, tx_count);
			display_timer = now;
			rx_count = 0;
			tx_count = 0;
		}
	}
}
