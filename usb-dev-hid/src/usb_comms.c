/**
 * @file usb_comms.c
 *
 * @brief USB HID interface
 *
 * @author Charles Oram
 *
 * @date Mar 2018
 *
 */

#include "hal.h"
#include <stdio.h>
#include "usb_comms.h"
#include "debug.h"

// USB descriptors

// EXFO provided VID, PID and product string
#define USB_VID 0x18C9
#define USB_PID 0x101F
#define USB_PRODUCT_STRING "Test Device"

// USB standard device descriptor
// Note: The STM32F4 uses little-endian ordering, which is what is
// required for USB descriptors.
static usb_device_descriptor_t usb_device_descriptor =
{
    sizeof(usb_device_descriptor_t),    /*bLength */
    USB_DEVICE_DESCRIPTOR_TYPE,         /*bDescriptorType*/
    0x0200,                             /*bcdUSB */
    0x00,                               /*bDeviceClass*/
    0x00,                               /*bDeviceSubClass*/
    0x00,                               /*bDeviceProtocol*/
    USB_FS_MAX_PACKET_SIZE,             /*bMaxPacketSize*/
	USB_VID,                            /*idVendor */
	USB_PID,                            /*idProduct */
    0x0200,                             /*bcdDevice rel. 2.00*/
    USBD_IDX_MFC_STR,                   /*Index of manufacturer  string*/
    USBD_IDX_PRODUCT_STR,               /*Index of product string*/
    USBD_IDX_SERIAL_STR,                /*Index of serial number string*/
    USBD_CFG_MAX_NUM                    /*bNumConfigurations*/
};

// Custom HID report descriptor
static uint8_t custom_hid_report_descriptor[] =
{
    0x06,0xFF,0xA0, //Usage Page (vendor defined)
    0x09,0x01,      //Usage (vendor defined)

    0xA1,0x01,      //Collection (Application)

    //The Input report
    0x09,0x03,      //Usage (vendor defined)
    0x85,HID_IN_REPORT_ID,      //Report ID
    0x15,0x00,      //Logical Minimum (0)
    0x26,0x00,0xFF, //Logical Maximum (255)
    0x95,USB_HID_REPORT_LEN,        //Report Count
    0x75,0x08,      //Report Size (8 bits)
    0x81,0x02,      //Input (Data, Variable, Absolute)

    // The Output report
    0x09,0x04,      //Usage (vendor defined)
    0x85,HID_OUT_REPORT_ID,      //Report ID
    0x15,0x00,      //Logical Minimum (0)
    0x26,0x00,0xFF, //Logical Maximum (255)
    0x95,USB_HID_REPORT_LEN,        //Report Count
    0x75,0x08,      //Report Size (8 bits)
    0x91,0x02,      //Output (Data, Variable, Absolute)

    0xC0, //End Collection
};
#define CUSTOM_HID_REPORT_DESC_SIZE sizeof(custom_hid_report_descriptor)

// USB HID device configuration descriptor
#define USB_CUSTOM_HID_CONFIG_DESC_SIZ 41
#define USB_CUSTOM_HID_DESC_SIZ 9
static uint8_t custom_hid_device_descriptor[USB_CUSTOM_HID_CONFIG_DESC_SIZ] =
{
  0x09,         /* bLength: Configuration Descriptor size */
  USB_CONFIGURATION_DESCRIPTOR_TYPE, /* bDescriptorType: Configuration */
  USB_CUSTOM_HID_CONFIG_DESC_SIZ,
  0x00,
  0x01,         /*bNumInterfaces: 1 interface*/
  0x01,         /*bConfigurationValue: Configuration value*/
  0x00,         /*iConfiguration: Index of string descriptor describing the configuration*/
  USB_BUS_POWERED,         /*bmAttributes: bus powered */
  (100/2),      /*MaxPower 100 mA: this current is used for detecting Vbus*/

  /************** Descriptor of Custom HID interface ****************/
  0x09,         /*bLength: Interface Descriptor size*/
  USB_INTERFACE_DESCRIPTOR_TYPE,/*bDescriptorType: Interface descriptor type*/
  0x00,         /*bInterfaceNumber: Number of Interface*/
  0x00,         /*bAlternateSetting: Alternate setting*/
  0x02,         /*bNumEndpoints*/
  USB_DEVICE_CLASS_HUMAN_INTERFACE,         /*bInterfaceClass*/
  0x00,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
  0x00,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
  0,            /*iInterface: Index of string descriptor*/

  /******************** Descriptor of Custom HID ********************/
  0x09,         /*bLength: HID Descriptor size*/
  CUSTOM_HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  CUSTOM_HID_REPORT_DESC_TYPE,      /*bDescriptorType*/
  CUSTOM_HID_REPORT_DESC_SIZE, /*wItemLength: Total length of Report descriptor*/
  0x00,

  /******************** Descriptor of Custom HID endpoints ***********/
  0x07,                         /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType: */
  USB_ENDPOINT_OUT(1),          /* bEndpointAddress */
  USB_ENDPOINT_TYPE_INTERRUPT,  /* bmAttributes: Interrupt endpoint */
  USB_HID_MAX_PACKET_SIZE,      /* maximum packet size */
  0x00,
  1,                            /* bInterval: Polling Interval (1 ms) */

  0x07,                         /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType: */
  USB_ENDPOINT_IN(1),           /* bEndpointAddress */
  USB_ENDPOINT_TYPE_INTERRUPT,  /* bmAttributes: Interrupt endpoint */
  USB_HID_MAX_PACKET_SIZE,      /* maximum packet size */
  0x00,
  1                             /* bInterval: Polling Interval (1 ms) */
};

// USB Language ID Device Descriptor
static lang_id_descriptor_t language_id_descriptor =
{
    sizeof(lang_id_descriptor_t),   /*bLength */
    USB_DESC_TYPE_STRING,                   /*bDescriptorType */
    USBD_LANGID_US                          /*wLANGID = US English */
};

// User defined strings
static char slot_id_string[4] = "3";
#define NUM_USER_STRINGS 1
user_strings_t user_strings_array[NUM_USER_STRINGS] =
{
    {0x10, slot_id_string}
};

// Main USB configuration, to pass to usb_init()
static usb_config_t usb_config =
{
    .device_descriptor = &usb_device_descriptor,
    .config_descriptor = custom_hid_device_descriptor,
    .config_descriptor_len = sizeof(custom_hid_device_descriptor),
    .lang_id_descriptor = &language_id_descriptor,
    .hid_report_descriptor = custom_hid_report_descriptor,
    .hid_report_descriptor_len = sizeof(custom_hid_report_descriptor),
    .manufacturer_string = "Coherent Solutions",
    .product_string = USB_PRODUCT_STRING,
    .configuration_string = "HID FS Config",
    .interface_string = "HID Interface",
    .serial_number_string = "?",    // The code will fill this in
    .user_strings = user_strings_array,
    .num_user_strings = NUM_USER_STRINGS,
    .in_endpoint_address = 0x81,
    .out_endpoint_address = 0x01,
    .in_max_packet_size = USB_HID_MAX_PACKET_SIZE,
    .out_max_packet_size = USB_HID_MAX_PACKET_SIZE,
    .in_report_len = USB_HID_REPORT_LEN,
    .out_report_len = USB_HID_REPORT_LEN,
    .in_report_id = HID_IN_REPORT_ID,
    .out_report_id = HID_OUT_REPORT_ID,

    // Device event callbacks
    .init_event_cb = NULL,
    .reset_cb = NULL,
    .configured_cb = NULL,
    .suspended_cb = NULL,
    .resumed_cb = NULL,
    .connected_cb = NULL,
    .disconnected_cb = NULL,
};

usb_dev_handle_t usb_hid_device;

usb_dev_handle_t usbcomms_init(char *serial_num)
{
    usb_config.serial_number_string = serial_num;
    usb_hid_device =  usb_init(&usb_config);
    return usb_hid_device;
}

