/**
 * @file usb_comms.h
 *
 * @brief USB HID interface
 *
 * @author Charles Oram
 *
 * @date Mar 2018
 *
 */

#ifndef _USB_COMS_H_
#define _USB_COMS_H_

#include "usb.h"

#define USB_HID_MAX_PACKET_SIZE     64
// Report length excluding the report ID
#define USB_HID_REPORT_LEN          63
#define HID_IN_REPORT_ID            1
#define HID_OUT_REPORT_ID           2

usb_dev_handle_t usbcomms_init(char *serial_num);

#endif
