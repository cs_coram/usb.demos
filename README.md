USB Demo Programs
=================
These programs can be compiled using System Workbench for STM32 on Windows.
They will run on the STM32F407G-DISCO "STM32F4 Discovery" board and the Waveshare
development boards.
They use the old style STM32 peripheral library v1.8.0 (which is included in this repo)
and they use the old style USB drivers "STM32 USB Host Device Library v2.2.0", but the
USB driver library files that are needed have just been copied into these projects in
the usb directory (and, for HID, have been modified).
The programs use mos, but to allow it to be easily compiled with System Workbench for STM32
I have just copied in the files from mos that I need.
They use UART2 for serial debug output.

HID
===

These two demos will send packets back and forward using HID.
Note that the USB Host HID driver still has a bug and will stop sending packets after a while.

usb-dev-hid
-----------
Send and receive custom HID packets to/from the host.

usb-host-hid
------------
Send and receive custom HID packets to/from the device.

Mass Storage (MSC)
=================

usb-dev-msc
-----------
This program lets the USB Device operate as a storage device (like a USB Flash drive)
using 100k of RAM as a disk drive.
The RAM is, of course, volatile, so you will need to reformat the drive each time
you connect it to a PC after a reset or power off.
If using this with usb-host-msc you need to first plug  it into a PC and format the drive.

usb-host-msc
------------
This program waits for a USB storage device to be plugged in to the USB bus and then waits for you to press the push button.
It will then display a directory listing on UART2 asnd wait for a button press.
Then it will write a file to the drive.
Then it will dump the contents of all files on the drive.

