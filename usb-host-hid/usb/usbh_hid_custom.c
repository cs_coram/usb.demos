#include "usbh_hid_custom.h"
#include <stdio.h>
#include <string.h>

static void  HID_Custom_Init (void);
static void  HID_Custom_Decode(uint8_t *data, uint16_t length);

extern int hid_packet_received;
extern int rx_count;

#pragma pack(4)
HID_cb_TypeDef HID_Custom_cb =
{
	HID_Custom_Init,
	HID_Custom_Decode
};

static void  HID_Custom_Init (void)
{

}

static char custom_hid_buf[65];
static void HID_Custom_Decode(uint8_t *pbuf, uint16_t length)
{
	memcpy(custom_hid_buf, pbuf, length);
	custom_hid_buf[length] = 0;
	//printf("Rx %s\r\n", custom_hid_buf);
	hid_packet_received = 1;
	rx_count++;
}

