
void debug_out_init(void);
void debug_out(char *str);
int debug_printf(char *fmt, ...);
void debug_out_ex(char *str, int len);
void debug_out_hex(uint8_t value);
void debug_dump_hex(char *header, uint8_t *buf, int length);
