#include "stm32f4xx.h"
#include "hal.h"
#include "hw.h"

#include "usbh_core.h"
#include "usbh_usr.h"
#include "usbh_hid_core.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "debug.h"

int hid_packet_received;
int rx_count = 0;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN USB_OTG_CORE_HANDLE      USB_OTG_Core __ALIGN_END;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN USBH_HOST                USB_Host __ALIGN_END;

void toggle_pin(gpio_pin_t *pin)
{
	gpio_set_pin(pin, !gpio_get_pin(pin));
}

void toggle_orange(void)
{
	toggle_pin(&orange_led_pd13);
}

void toggle_red(void)
{
	toggle_pin(&red_led_pd14);
}

int main(void)
{
	uint32_t timer;
	uint32_t now;
	int len;
	char buf[10];
	int tx_count = 0;
	uint32_t display_timer;

	/*!< At this stage the microcontroller clock setting is already configured,
	   this is done through SystemInit() function which is called from startup
	   files (startup_stm32f40_41xxx.s/startup_stm32f427_437xx.s/startup_stm32f429_439xx.s)
	   before to branch to application main.
	   To reconfigure the default setting of SystemInit() function, refer to
	   system_stm32f4xx.c file.
	 */
	sys_init();
	debug_out_init();

	gpio_init_pin(&green_led_pd12);
	gpio_set_pin(&green_led_pd12, 0);
	gpio_init_pin(&red_led_pd14);
	gpio_set_pin(&red_led_pd14, 0);
	gpio_init_pin(&blue_led_pd15);
	gpio_set_pin(&blue_led_pd15, 0);
	gpio_init_pin(&orange_led_pd13);
	gpio_set_pin(&orange_led_pd13, 0);

	// User push button PA0
	gpio_init_pin(&pb_pa0);

	/* Output a message on Hyperterminal using printf function */
	debug_printf("\n\rStart up USB Host MSC example\n\r");

	// Wait for the message to be sent to the UART
	sys_spin(1000);

	// Put the report ID in the packet
	buf[0] = 2;

	/* Init Host Library */
	USBH_Init(&USB_OTG_Core,
#ifdef USE_USB_OTG_FS
            USB_OTG_FS_CORE_ID,
#else
            USB_OTG_HS_CORE_ID,
#endif
            &USB_Host,
            &HID_cb,
            &USR_cb);

	// Kick off sending packets
	hid_packet_received = 1;

	timer = sys_get_tick();
	display_timer = timer;
	while (1)
	{
		/* Host Task handler */
		USBH_Process(&USB_OTG_Core, &USB_Host);

		now = sys_get_tick();
		if ((uint32_t)(now - timer) > 50)
		{
			timer = now;
			toggle_pin(&green_led_pd12);
			sprintf(&buf[1], "%d", tx_count);
			len = strlen(&buf[1]) + 1;
			if (USBH_HID_SendPacket(&USB_OTG_Core, (uint8_t *)buf, len))
			{
				tx_count++;
				//debug_printf("TX %s\r\n", &buf[1]);
				toggle_pin(&blue_led_pd15);
			}
		}
		// Display the receive and transmit counts every2 s
		if ((uint32_t)(now - display_timer) > 2000)
		{
			debug_printf("Rx %d Tx %d\r\n", rx_count, tx_count);
			display_timer = now;
			rx_count = 0;
			tx_count = 0;
		}
	}
}
