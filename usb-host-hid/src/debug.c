

#include <stm32f4xx_conf.h>
#include <gpio_hw.h>
#include <uart_hw.h>
#include <dma_hw.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include "hw.h"

#ifdef DEBUG_OUT

//#define USE_ITM_OUTPUT

#ifdef USE_ITM_OUTPUT

// Not working yet!

// Run these commands in gdb:
// monitor itm port 0 on
// monitor tpiu config internal itm.fifo uart off 8000000
// Then use "cat itm.fifo" to read the output.
// itm.fifo is in the directory where openocd is running.

void debug_out_init(void)
{
    //DBGMCU->CR = 0x00000020;
    /* STM32 specific configuration to enable the TRACESWO IO pin */
    RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
    AFIO->MAPR |= (2 << 24); // Disable JTAG to release TRACESWO
    DBGMCU->CR |= DBGMCU_CR_TRACE_IOEN; // Enable IO trace pins

    if (!(DBGMCU->CR & DBGMCU_CR_TRACE_IOEN))
    {
        // Some (all?) STM32s don't allow writes to DBGMCU register until
        // C_DEBUGEN in CoreDebug->DHCSR is set. This cannot be set by the
        // CPU itself, so in practice you need to connect to the CPU with
        // a debugger once before resetting it.
        return;
    }

    /* Configure Trace Port Interface Unit */
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; // Enable access to registers
    TPI->ACPR = 0; // Trace clock = HCLK/(x+1) = 8MHz
    TPI->SPPR = 2; // Pin protocol = NRZ/USART
    TPI->FFCR = 0x102; // TPIU packet framing enabled when bit 2 is set.
                       // You can use 0x100 if you only need DWT/ITM and not ETM.

    /* Configure PC sampling and exception trace  */
    DWT->CTRL = (1 << DWT_CTRL_CYCTAP_Pos) // Prescaler for PC sampling
                                           // 0 = x32, 1 = x512
              | (0 << DWT_CTRL_POSTPRESET_Pos) // Postscaler for PC sampling
                                                // Divider = value + 1
              | (1 << DWT_CTRL_PCSAMPLENA_Pos) // Enable PC sampling
              | (2 << DWT_CTRL_SYNCTAP_Pos)    // Sync packet interval
                                               // 0 = Off, 1 = Every 2^23 cycles,
                                               // 2 = Every 2^25, 3 = Every 2^27
              | (1 << DWT_CTRL_EXCTRCENA_Pos)  // Enable exception trace
              | (1 << DWT_CTRL_CYCCNTENA_Pos); // Enable cycle counter

    /* Configure instrumentation trace macroblock */
    ITM->LAR = 0xC5ACCE55;
    ITM->TCR = (1 << ITM_TCR_TraceBusID_Pos) // Trace bus ID for TPIU
             | (1 << ITM_TCR_DWTENA_Pos) // Enable events from DWT
             | (1 << ITM_TCR_SYNCENA_Pos) // Enable sync packets
             | (1 << ITM_TCR_ITMENA_Pos); // Main enable for ITM
    ITM->TER = 0xFFFFFFFF; // Enable all stimulus ports
}

void debug_out_ex(char *str, int len)
{
    if (((ITM->TCR & ITM_TCR_ITMENA_Msk) != 0UL) &&      /* ITM enabled */
        ((ITM->TER & 1UL               ) != 0UL)   )     /* ITM Port #0 enabled */
    {
        while (len)
        {
            while (ITM->PORT[0U].u32 == 0UL)
            {
                __NOP();
            }
            ITM->PORT[0U].u8 = (uint8_t)*str;
            len--;
            str++;
        }
  }
}

#else

#define MAX_DEBUG_OUT_LEN 80
static char debug_out_buf[MAX_DEBUG_OUT_LEN];

static volatile int tx_busy = false;
static void tx_complete(uart_t *uart, void *buf, uint16_t len, void *param)
{
    tx_busy = false;
}

void debug_out_init(void)
{
    uart_init(&debug_uart);
}

void debug_out_ex(char *str, int len)
{
    if (len > MAX_DEBUG_OUT_LEN)
    {
        len = MAX_DEBUG_OUT_LEN;
    }
    while (tx_busy)
    {
    }
    tx_busy = true;
    memcpy(debug_out_buf, str, len);
    uart_write(&debug_uart, debug_out_buf, len, tx_complete, NULL);
}

int debug_printf(char *fmt, ...)
{
    va_list ap;
    int r;

    while (tx_busy)
    {
    }
    tx_busy = true;

    va_start(ap, fmt);
    r = vsnprintf(debug_out_buf, MAX_DEBUG_OUT_LEN, fmt, ap);
    va_end(ap);
    uart_write(&debug_uart, debug_out_buf, r, tx_complete, NULL);

    return r;
}

#endif

void debug_out(char *str)
{
    int len = strlen(str);
    debug_out_ex(str, len);
}

void to_hex(uint8_t value, char *buf)
{
    uint8_t nibble;

    nibble = (value >> 4);
    if (nibble < 10)
    {
        *buf = nibble + '0';
    }
    else
    {
        *buf = nibble + 'A' - 10;
    }
    ++buf;
    nibble = value & 0x0F;
    if (nibble < 10)
    {
        *buf = nibble + '0';
    }
    else
    {
        *buf = nibble + 'A' - 10;
    }
    ++buf;
    *buf = 0;
}

void debug_out_hex(uint8_t value)
{
    char hex_buf[4];

    to_hex(value, hex_buf);
    debug_out(hex_buf);
}

void debug_dump_hex(char *header, uint8_t *buf, int length)
{
    int i;
    int width;

    debug_out(header);
    debug_out(": ");
    width = 0;
    for (i=0; i<length; i++)
    {
        debug_out_hex(*buf);
        ++buf;
        debug_out_hex(*buf);
        ++width;
        if (width >= 40)
        {
            width = 0;
            debug_out("\r\n");
        }
    }
    if (width != 0)
    {
        debug_out("\r\n");
    }
}

// Define _write() so that printf works
int _write(int file, char *ptr, int len)
{
    if ((file != STDOUT_FILENO) && (file != STDERR_FILENO))
    {
        return -1;
    }
    debug_out_ex(ptr, len);
    return 0;
}

#else

// No debug UART - define dummy functions

void debug_out_init(void)
{
}

void debug_out(char *str)
{
}

int debug_printf(char *fmt, ...)
{
    return 0;
}

void debug_out_ex(char *str, int len)
{
}

void debug_out_hex(uint8_t value)
{
}

void debug_dump_hex(char *header, uint8_t *buf, int length)
{
}

#endif
